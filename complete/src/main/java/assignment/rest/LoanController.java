package assignment.rest;

import assignment.Messages;
import assignment.Params;
import assignment.RiskChecker;
import assignment.models.GetLoanResponse;
import assignment.models.Loan;
import assignment.utils.WebUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;


@RestController
public class LoanController {

    private final AtomicLong idCounter = new AtomicLong();
    public static ArrayList<Loan> loans = new ArrayList<>();
    private static RiskChecker riskChecker;

    @RequestMapping(value = "/getLoan", method = RequestMethod.POST)
    public GetLoanResponse getLoan(@RequestParam(value = "userName") String userName,
                                   @RequestParam(value = "userSurname") String userSurname,
                                   @RequestParam(value = "amount") double amount,
                                   @RequestParam(value = "term") long term) {
        String clientIpAddress = new WebUtils().getClientIp();
        Params.ipAddresses.add(clientIpAddress);

        if (riskChecker == null) {
            riskChecker = new RiskChecker();
        }

        boolean timeRisk = riskChecker.checkTimeRisk();
        boolean ipRisk = riskChecker.ipRecurrenceRisk(clientIpAddress);

        if (!timeRisk && !ipRisk) {
            loans.add(new Loan(idCounter.incrementAndGet(), userName, userSurname, amount, term, new Date()));
            return new GetLoanResponse(Messages.loanSuccess);
        } else if (!timeRisk && ipRisk) {
            return new GetLoanResponse(Messages.ipRecurrence);
        } else if (timeRisk && !ipRisk) {
            return new GetLoanResponse(Messages.timeOfDayRejection);
        } else {
            return new GetLoanResponse(Messages.loanRejected);
        }
    }

    @RequestMapping(value = "/getLoanHistory", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ArrayList<Loan> getLoanHistory(@RequestParam(value = "userName") String userName,
                                          @RequestParam(value = "userSurname") String userSurname) {
        ArrayList<Loan> toShow = new ArrayList<>();
        for (int i = 0; i < loans.size(); i++) {
            Loan loan = loans.get(i);
            if (loan.getUserName().equals(userName) && loan.getUserSurname().equals(userSurname)) {
                toShow.add(loan);
            }
        }
        return toShow;
    }
}
//getLoan?userName=test&userSurname=test2&amount=1000&term=1
//getLoanHistory?userName=test&userSurname=test2