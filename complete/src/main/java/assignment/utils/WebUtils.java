package assignment.utils;

import assignment.Params;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class WebUtils {

    private HttpServletRequest request;

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public String getClientIp() {
        if (request == null) {
            return "";
        }
        String remoteAddress = request.getHeader("X-FORWARDED-FOR");
        if (remoteAddress.isEmpty() || "".equals(remoteAddress)) {
            remoteAddress = request.getRemoteAddr();
        }
        return remoteAddress;
    }

    @Scheduled(cron = "0 0 * * * *")
    public void resetIpAddress() {
        Params.ipAddresses.clear();
    }


}
