package assignment;

public class Messages {

    public static final String loanSuccess = "Loan has been issued";
    public static final String loanRejected = "Loan has not been issued";
    public static final String ipRecurrence = "You have already been issued maximum loans per day";
    public static final String timeOfDayRejection = "You cannot get a loan at this time";
}
