package assignment;

import assignment.Params;

import java.util.Calendar;
import java.util.Date;

public class RiskChecker {

    public static final int maxIpAddressRecurrence = 3;
    public static final int timeOfDayRiskStart = 0;
    public static final int timeOfDayRiskEnd = 6;

    public boolean checkTimeRisk() {
        boolean risk = false;
        int time = timeNow();

        if (time > timeOfDayRiskStart && time < timeOfDayRiskEnd) {
            risk = true;
        }
        return risk;
    }

    public boolean ipRecurrenceRisk(String ipAddress) {
        boolean risk = false;
        int countIpRecurrence = 0;

        for (int i = 0; i < Params.ipAddresses.size(); i++) {
            if (ipAddress.equals(Params.ipAddresses.get(i))) {
                countIpRecurrence++;
            }
        }
        if (countIpRecurrence > maxIpAddressRecurrence) {
            risk = true;
        }
        return risk;
    }

    int timeNow() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.HOUR_OF_DAY);
    }
}
