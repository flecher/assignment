package assignment.models;

import java.util.Date;

public class Loan {

    private final long id;
    private final String userName;
    private final String userSurname;
    private final double amount;
    private final long term;
    private final Date date;

    public Loan(long id, String userName, String userSurname, double amount, long term, Date date) {
        this.id = id;
        this.userName = userName;
        this.userSurname = userSurname;
        this.amount = amount;
        this.term = term;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public double getAmount() {
        return amount;
    }

    public long getTerm() {
        return term;
    }

    public Date getDate() {
        return date;
    }

}
