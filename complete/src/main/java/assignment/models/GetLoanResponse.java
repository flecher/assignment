package assignment.models;

/**
 * Created by marius on 17.10.16.
 */
public class GetLoanResponse {

    private final String response;

    public GetLoanResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

}
