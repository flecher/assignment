Run this project with Spring Boot.

HTTP GET request at:
http://localhost:8080/getLoanHistory?userName=name&userSurname=username
----


Successful requests will respond with

{"id":1,"userName":"Name","userSurname":"Surname","amount":"1000","term":"1","date":"1508135993"}

----

HTTP POST request at:

http://localhost:8080/getLoan?userName=name&userSurname=username&amount=1000&term=12

----

Successful requests will respond with

{"response":"Success"}

----